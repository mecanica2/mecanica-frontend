import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { ToolbarModule } from 'primeng/toolbar';
import { SplitButtonModule } from 'primeng/splitbutton';
import { CardModule } from 'primeng/card';
import { PresentationSectionComponent } from './presentation-section/presentation-section.component';
import { AboutSectionComponent } from './about-section/about-section.component';
import { ButtonModule } from 'primeng/button';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';

import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { FeatureSectionComponent } from './feature-section/feature-section.component';
import { AccessSectionComponent } from './access-section/access-section.component';
import { FooterComponent } from './footer/footer.component';

export const playerFactory = (): any => {
  return import('lottie-web');
};

@NgModule({
  declarations: [
    HeaderComponent,
    PresentationSectionComponent,
    AboutSectionComponent,
    AccessSectionComponent,
    FeatureSectionComponent,
    RegisterComponent,
    LoginComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    ToolbarModule,
    ButtonModule,
    SplitButtonModule,
    LottieAnimationViewModule,
    LottieModule.forRoot({ player: playerFactory }),
    CardModule,
  ],
  exports: [
    HeaderComponent,
    PresentationSectionComponent,
    AboutSectionComponent,
    AccessSectionComponent,
    FeatureSectionComponent,
    FooterComponent,
  ],
})
export class ComponentsModule {}
