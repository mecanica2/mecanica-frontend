import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-presentation-section',
  templateUrl: './presentation-section.component.html',
  styleUrls: ['./presentation-section.component.scss'],
})
export class PresentationSectionComponent implements OnInit {
  private readonly presentationText = `
  IDevS é o Programa de Formação DEV da IDS, uma empresa de Pato Branco/PR que desenvolve soluções em tecnologia tornando o sistema público mais eficaz e humanizado!
  `;
  constructor() {}

  ngOnInit(): void {}

  showModal(): void {
    Swal.fire(this.presentationText);
  }
}
